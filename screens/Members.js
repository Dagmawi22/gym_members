// Aboutscreen.js
import React, { Component, useState, useEffect } from "react";
import { Button, View, Text } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { FontAwesome5 } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { Octicons } from '@expo/vector-icons';

export default function MembersScreen() {

  const [members, setMembers] = useState([])

  const [active, setActive] = useState([])
  const [activeCount, setActiveCount] = useState(0)

  const [expired, setExpired] = useState([])
  const [expiredCount, setExpiredCount] = useState(0)

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('user')
      console.log(JSON.parse(jsonValue))
      setMembers(JSON.parse(jsonValue))

      let ac = 0
      let a = []

      let ec = 0
      let e = []

      for (let i=0; i<JSON.parse(jsonValue).length; i++){
        if(
          (
            JSON.parse(jsonValue)[i].expiryD >= new Date().getDate() && 
            JSON.parse(jsonValue)[i] >= new Date().getMonth() &&
            JSON.parse(jsonValue)[i] >= new Date().getFullYear())
        ){
         ac = ac+1
         a.push(JSON.parse(jsonValue)[i])

        }else{
          ec = ac+1
          e.push(JSON.parse(jsonValue)[i])
        }
        setActiveCount(ac)
        setActive(a)
        setExpiredCount(ec)
        setExpired(e)
      }
     
    } catch(e) {
      // error reading value
    }
  }

  useEffect(() => {
    
   getData()
   }, []); // Only re-run the effect if count changes
  return (
    <View style={{ backgroundColor:"white"}}>
      <Text 
      style={{
        // fontSize:37,
        marginBottom:20,
        textAlign:"center"
      }}
      >
        <Ionicons name="people" size={40} color="black" />
         &nbsp;&nbsp;<sup style={{marginTop:"-30px", color:"white", backgroundColor:"#123456", fontSize:20, padding:"2px 4px", borderRadius:"15px"}}>{activeCount+expiredCount}</sup>

         &nbsp;&nbsp; <Octicons name="dot-fill" size={44} color="green" style={{cursor:"pointer"}} onClick={()=>{
          setMembers(active)
         }}/>
         &nbsp;<sup style={{marginTop:"-30px", color:"white", backgroundColor:"#123456", fontSize:20, padding:"2px 4px", borderRadius:"15px"}}>{activeCount}</sup>
       
         &nbsp;&nbsp; <Octicons name="dot-fill" size={44} color="red" style={{cursor:"pointer"}} onClick={()=>{
          setMembers(expired)
         }}/>
         &nbsp;<sup style={{marginTop:"-30px", color:"white", backgroundColor:"#123456", fontSize:20, padding:"2px 4px", borderRadius:"15px"}}>{expiredCount}</sup>
       
       </Text> 
      
        {
        members.map((member, index)=> (
          <div key={index}
              style={{
                backgroundColor:"white",
                height:"330px",
                marginLeft:"5vw",
                width:"90vw",
                marginRight:"5vw",
                padding:"",
                display:"flex",
                alignItems:"center",
                justifyContent:"center",
                shadowColor: '#123456',
                shadowOffset: {width: 0, height: 1},
                shadowOpacity: 1,
                shadowRadius: 15,
                marginBottom:50,
                border:"1px solid #e6e6f2",
                borderRadius:"30px",
              }}
          >
            <div
            style={{
              display:"flex",
              flexDirection:"column",
              marginTop:10,
            }}
            >
           <div
           style={{
            backgroundColor:"#123456",
            width:"60px",
            height:"60px",
            margin:"0 auto",
            borderRadius:"100%",
            textAlign:"center",
            display:"flex",
            alignItems:"center",
            justifyContent:"center",
            marginTop:30
           }}
           >
            <span 
            style={{
              color:"white",
              fontSize:30
            }}
            >{member.name.substring(0, 1)}</span>
           </div>
           <div style={{margin:"0 auto", marginTop:"-7px"}}>
              {
                (
                member.expiryD >= new Date().getDate() && 
                member.expiryM >= new Date().getMonth() &&
                member.expiryY >= new Date().getFullYear())
                ?
                <>
                <Octicons name="dot-fill" size={44} color="green" />
                </>
                :
                <>
                <Octicons name="dot-fill" size={44} color="red" />
                </>
              }
            </div>
    
         <div style={{}}>
            <h2 style={{ marginLeft:20}}>
            <Ionicons name="person-circle-sharp" size={24} color="black" />
            &nbsp;&nbsp;
              {member.name}</h2>
           </div>

           <div style={{marginTop:"-20px"}}>
            <h2 style={{ marginLeft:20}}>
            <Feather name="phone-call" size={24} color="black" />
             &nbsp;&nbsp;
              {member.phone}
              </h2>
           </div>

           <div style={{marginTop:"-20px", marginLeft:20, marginBottom:20}}>
            <h2>
            <MaterialIcons name="date-range" size={24} color="black" />
            &nbsp;&nbsp;
              {member.expiryY+'/'+member.expiryM+'/'+member.expiryD+' - '+member.expiryY+'/'+member.expiryM+'/'+member.expiryD+'  '}
            
            {/* <FontAwesome name="check-circle" size={34} color="green" /> */}
            {/* <FontAwesome name="close" size={24} color="red" /> */}
              </h2>
           </div>

            </div>
          </div>
        ))
      }
      {
        members.length==0 &&
        <>
        <div
        style={{
          position:"fixed",
          top:"50%",
          left:"50%",
          transform:"translate(-50%, -50%)",
          fontSize:20
        }}
        
        >
          <FontAwesome5 name="user-alt-slash" size={54} color="#123456" style={{textAlign:"center"}}/>
          
        </div>
          <Text
        style={{
          position:"fixed",
          top:"50%",
          left:"50%",
          transform:"translate(-50%, -50%)",
          fontSize:20,
          marginTop:60,
          color:"#123456"
        }}
        
        >
          
          No member found!
          </Text>
          </>
      }
     

    </View>
  );
}