// Homescreen.js
import React from "react";
import { SafeAreaView, Text, View, StyleSheet, Image } from 'react-native';
import { useEffect, useState } from "react";
import Ionicons from '@expo/vector-icons/Ionicons';
import { FontAwesome5 } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';
import {Dimensions} from 'react-native';

export default function HomeScreen({ navigation }) {

  const [ready, setReady] = useState("none")
  const [logo, setLogo] = useState("block")
  const [title, setTitle] = useState(false)
  const [menu, setMenu] = useState("none")
  const [add, setAdd] = useState("none")
  const [apiResponse, setApiResponse] = useState(null)

  React.useLayoutEffect(() => {
    navigation.setOptions({headerShown: false});
  }, [navigation]);


  useEffect(() => {
 setTimeout(
  function() {
      setReady("");
      setLogo("none");
      setTitle(true)
      setAdd("")
  }
  .bind(this),
  3000
);
// fetch('http://localhost:8000/api/login', {
//     method: 'POST',
//     headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({ "phone": 918888225, "password": "1234" })
// })
// .then(response => response.json())
// .then(response => setApiResponse(response.user.name)) 

const storeData = async (value) => {
  // await AsyncStorage.setItem('user', JSON.stringify(
  //   []
  // ))
    getData()
  }

const getData = async () => {
  try {
    const jsonValue = await AsyncStorage.getItem('user')
    console.log("vallllll", JSON.parse(jsonValue))
    let val = JSON.parse(jsonValue)
  } catch(e) {
    await AsyncStorage.setItem('user', '')
  }
}

storeData()


}, []); // Only re-run the effect if count changes
  return (
    <View style={{backgroundColor:"#f6f6fb", height:"100%", paddingTop:0}}>
   
      <View style={{
        position:"absolute",
        top:"50%",
        left:"50%",
        transform:"translate(-50%,-50%)",
        display:`${logo}`
      }}>
        {/* <Ionicons name="logo-xbox" size={100} color="#123456" /> */}
        <MaterialCommunityIcons name="weight-lifter" size={100} color="#123456" />

      </View>

      <View style={{display:`${ready}`}}>

       <View style={{display:`${menu}`, zIndex:100}}>
        <View 
        style={styles.shadow}
        >
       <View onClick={() => setMenu("none")}>
        <AntDesign name="menufold" size={25} color="white" style={{marginLeft:220, marginTop:5, cursor:"pointer"}}/>  
       </View>      
       <Image source={require('./male.webp')} style={styles.avatar} />
        <Text style={{textAlign:"center", color:"white", fontSize:18}}>Abebe Kebede</Text>
        

        <View style={{height: '2px', marginTop:20, marginLeft:0, backgroundColor: 'white'}} />
        
        <View style={{display:"flex", flexDirection:"row", marginTop:20}}>
        <AntDesign name="profile" size={17} color="white" style={{marginLeft:5}}/>
        <Text style={{fontSize:18, marginLeft:20, marginTop:1, color:"white"}}>Profile</Text>
        </View>

        <View style={{display:"flex", flexDirection:"row", marginTop:20}}>
        <Feather name="package" size={17} color="white" style={{marginLeft:5}}/>
        <Text style={{fontSize:18, marginLeft:20, marginTop:1, color:"white"}}>Orders</Text>
        </View>

        <View style={{display:"flex", flexDirection:"row", marginTop:20}}>
        <Entypo name="location" size={17} color="white" style={{marginLeft:5}}/>
        <Text style={{fontSize:18, marginLeft:20, marginTop:1, color:"white"}}>Track orders</Text>
        </View>
        </View>
        </View>
        <View>
          {/* <Text onClick={() => setMenu("")} style={{position:"fixed", bottom:5, left:"47vw", cursor:"pointer",
          transform:"rotate(270deg)"
        }}>
          <AntDesign name="menuunfold" size={24}  color="#123456" />
          </Text> */}
        </View>
       
        <View style={{display:"", marginTop:0, alignItems:"center", justifyContent:"center",
        backgroundColor:"white", height:140, width:"100vw", borderTopRightRadius:""   
      }}
        >
         <MaterialCommunityIcons name="weight-lifter" size={150} color="#123456" />
        </View>

        {/* <View style={{position:"absolute", top:0, right:0, alignItems:"center", justifyContent:"center",
        backgroundColor:"#123456", height:140, width:"25vw", borderTopLeftRadius:"50%"   
      }}
        >
          <MaterialCommunityIcons name="forklift" size={54} color="white" />
        </View> */}
         <BarChart
        data={{
          labels: ['January 2023', 'February 2023'],
          datasets: [
            {
              data: [30, 45],
            },
          ],
        }}
        width={Dimensions.get('window').width - 20}
        height={220}
        yAxisLabel={'Rs'}
        chartConfig={{
          backgroundColor: 'white',
          backgroundGradientFrom: '#123456',
          backgroundGradientTo: '#123456',
          decimalPlaces: 2,
          // color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
          color: (opacity = 1) => `white`,
          style: {
            borderRadius: 16,
          },
          }}
          style={{
            marginVertical: 8,
            borderRadius: 16,
            marginLeft:10,
          }}
      />
        
        <View
        style={{
        marginTop:"10vh"
    
    }}
        >
        
          
      <View 
          onClick={() => navigation.navigate("CheckMember")}
          style={{backgroundColor:"white", height:"150px", width:"150px", position:"absolute", left:30,
        alignItems:"center", justifyContent:"center",
        shadowColor: '#123456',
          shadowOffset: {width: 0, height: 1},
          shadowOpacity: 1,
          shadowRadius: 1,
          cursor:"pointer",
          borderRadius:"15px"
        }}>
        <MaterialIcons name="person-search" size={54} color="#123456" />
             <Text style={{fontSize:18, marginTop:10, color:"#123456"}}>Search</Text>
          </View>

          <View 
           onClick={() => navigation.navigate("MembersScreen")}
          style={{backgroundColor:"white", height:"150px", width:"150px", position:"absolute", right:30,
          alignItems:"center", justifyContent:"center",
          shadowColor: '#123456',
          shadowOffset: {width: 0, height: 1},
          shadowOpacity: 1,
          shadowRadius: 1,
          cursor:"pointer",
          borderRadius:"15px"
        }}
        >
            <FontAwesome5 name="users" size={54} color="#123456" />
             <Text style={{fontSize:18, marginTop:10, color:"#123456"}}>Members</Text>
          </View>

         

        </View>

         <View
        style={{display:"flex", flexDirection:"row", marginTop:20}}
        >
         
      </View>
      </View>

     

      <View
      style={{
        position:"absolute",
        bottom:"1.2em",
        right:10,
        backgroundColor:"#123456",
        padding:10,
        borderRadius:"100%",
        cursor:"pointer",
        display:`${add}`
      }}
      onClick={() => navigation.navigate("AddMember")}
      >
        <Entypo name="add-user" size={22} color="white" />

      </View>
      
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: '#f6f6fb',
    display:'flex',
    flexDirection:'row'
  },
  paragraph: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 20,
    backgroundColor:'#fff',
    border:'1px solid black',
    width:100,
    height:100,
    textAlign:'justify',
    marginLeft:'50px',
    cursor:'pointer'
  },
  paragraph1: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 20,
    backgroundColor:'#fff',
    border:'1px solid black',
    width:100,
    height:100,
    textAlign:'justify',
    cursor:'pointer'
  },
  main: {
    width:'100vw',
    height:'100vh',
    backgroundColor:'white',
    justifyContent:'center',
    alignItems:'center'
  },
  shadow: {  
    shadowColor: '#123456',
          shadowOffset: {width: 0, height: 1},
          shadowOpacity: 1,
          shadowRadius: 15,
          display:"", 
          position:"fixed", 
          top:0, 
          left:0, 
          width:"250px", 
          height:"100vh", 
          backgroundColor:"#123456",
          color:"white",
          zIndex:100,
          
  },
  avatar: {
    height:100,
    width:100, 
    marginLeft:75, 
    marginTop:50, 
    marginBottom:10, 
    borderRadius:"100%",
    shadowColor: '#123456',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 1,
    shadowRadius: 15,
  },
 
});