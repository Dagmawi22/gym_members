import React, { useState, useEffect } from "react";
import { View, Picker, StyleSheet, Text, TextInput, TouchableOpacity } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';

const App = () => {
  const [selectedValue, setSelectedValue] = useState("current");
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [day, setDay] = useState("");
  const [month, setMonth] = useState("");
  const [year, setYear] = useState("");

  const saveMember = async () => {
    if(name && year && month && day && phone){
      let newM = {
      "name": name,
      "expiryY": year,
      "expiryM": month,
      "expiryD": day,
      "startY": year,
      "startM": month,
      "startD": day,
      "phone": phone
    }
    let jValue = await AsyncStorage.getItem('user')
    if(jValue){
      jValue = JSON.parse(jValue)
      jValue.push(newM)
      await AsyncStorage.setItem('user', JSON.stringify(jValue))
      getData()
    }else{
      
    await AsyncStorage.setItem('user', JSON.stringify([newM,]))
    getData()
    }
   
    }
    
  }

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('user')
      console.log(JSON.parse(jsonValue))
      setAsyncData(JSON.parse(jsonValue))
    } catch(e) {
      console.log("error reading value")
    }

  }

  useEffect(() => {
    getData()
   }, []);

  return (
    <View style={styles.container}>
     <Text style={{fontSize:17, marginBottom:10}}>Name</Text>
      {/* <Picker
        selectedValue={selectedValue}
        style={{ height: 50, width: "100%", backgroundColor:"white", border:"none", textAlign:"center"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
      >
        <Picker.Item label="Your location" value="current" />
        <Picker.Item label="Other location" value="other" />
      </Picker> */}
       <TextInput
        value={name}
        placeholder="eg: Abe Kebe"
        placeholderTextColor="#e6e6ff"
        style={{ height: 50, width: "100%", backgroundColor:"white", border:"none", textAlign:"left", paddingLeft:10, borderRadius:"15px"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setName(newText)}
      ></TextInput>

      <Text style={{fontSize:17, marginBottom:10, marginTop:20}}>Package ends on:</Text>

      <View style={{display:"flex", flexDirection:"row"}}>

      <View style={{display:"flex", flexDirection:"column"}}>

      <Text style={{fontSize:17, marginBottom:10, marginTop:10}}>DD</Text>
      <TextInput
        placeholder="eg: 01"
        placeholderTextColor="#e6e6ff"
        value={day}
        maxLength={2}
        keyboardType="numeric"
        style={{ height: 50, width: "20vw", backgroundColor:"white", border:"none", textAlign:"left", paddingLeft:10,  borderRadius:"15px"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setDay(newText)}
      >
        
      </TextInput>
      </View>

      <View style={{display:"flex", flexDirection:"column", marginLeft:"10vw"}}>

      <Text style={{fontSize:17, marginBottom:10, marginTop:10}}>MM</Text>
      <TextInput
        placeholder="eg: 12"
        placeholderTextColor="#e6e6ff"
        keyboardType="numeric"
        maxLength={2}
        value={month}
        style={{ height: 50, width: "20vw", backgroundColor:"white", border:"none", textAlign:"left", paddingLeft:10, borderRadius:"15px"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setMonth(newText)}
      >
        
      </TextInput>
      </View>

      <View style={{display:"flex", flexDirection:"column", marginLeft:"10vw"}}>

      <Text style={{fontSize:17, marginBottom:10, marginTop:10}}>YYYY</Text>
      <TextInput
      placeholder="eg: 2024"
      placeholderTextColor="#e6e6ff"
       keyboardType="numeric"
        value={year}
        maxLength={4}
        style={{ height: 50, width: "30vw", backgroundColor:"white", border:"none", textAlign:"left", paddingLeft:10, borderRadius:"15px"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setYear(newText)}
      >
        
      </TextInput>
        </View>
      </View>

      <Text style={{fontSize:17, marginBottom:10, marginTop:20}}>Phone</Text>

      <TextInput
        value={phone}
        keyboardType="numeric"
        maxLength={10}
        placeholder="eg: 0900010203"
        placeholderTextColor="#e6e6ff"
        style={{ height: 50, width: "100%", backgroundColor:"white", border:"none", textAlign:"left", paddingLeft:10, borderRadius:"15px"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setPhone(newText)}
      ></TextInput>

      {/* <Text style={{fontSize:17, marginBottom:10, marginTop:10}}>Image</Text> */}
     <View style={{display:"flex", flexDirection:"row"}}>
      {/* <TextInput
        value={year}
        style={{ height: 50, width: "80vw", backgroundColor:"white", border:"none", textAlign:"center"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setYear(newText)}
      >
        
      </TextInput> */}
      {/* <Text style={{display:"flex", alignItems:"center", justifyContent:"center", backgroundColor:"#123456", width:"10vw", color:"white"}}>
        kg
      </Text> */}
      </View>

      <TouchableOpacity
        style={{backgroundColor:"#123456", color:"white", width:"100%", marginTop:40, height:50, alignItems:"center", justifyContent:"center", borderRadius:"15px"}}
        onPress={()=>saveMember()}
      >
        <Text style={{color:"white", fontSize:16}}>Add</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: "start"
  }
});

export default App;

