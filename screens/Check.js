import React, { useState, useEffect } from "react";
import { View, Picker, StyleSheet, Text, TextInput, TouchableOpacity } from "react-native";
import { FontAwesome } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Ionicons } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { Octicons } from '@expo/vector-icons';

const App = () => {

  const [phone, setPhone] = useState("");
  const [members, setMembers] = useState([])
  const [showMember, setShowMember] = useState(false)
  const [allMembers, setAllMembers] = useState([])
  const [loader, setLoader] = useState(false)
  const [viewResult, setViewResult] = useState(false)
  const [edit, setEdit] = useState(false)
  const [searchPhone, setSearchPhone] = useState("")


  const [newName, setNewName] = useState("")
  const [newPhone, setNewPhone] = useState("")
  const [newExpiryD, setNewExpiryD] = useState("")
  const [newExpiryM, setNewExpiryM] = useState("")
  const [newExpiryY, setNewExpiryY] = useState("")


  const [editedMember, setEditedMember] = useState([])

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('user')
      console.log(JSON.parse(jsonValue))
      setAllMembers(JSON.parse(jsonValue))
    } catch(e) {
      console.log("error reading value")
    }

  }

  const replaceUserData = async () => {
    let newM = {
      "name": newName,
      "expiryY": newExpiryY,
      "expiryM": newExpiryM,
      "expiryD": newExpiryD,
      "startY": "",
      "startM": "",
      "startD": "",
      "phone": phone
    }

    let member =  allMembers.filter(
      (members) => members.phone == searchPhone
    )


    try {
      let jValue = await AsyncStorage.getItem('user')
      jValue = JSON.parse(jValue)
     
      for(let i=0; i<jValue.length; i++){
        if(jValue[i].phone == searchPhone){
          jValue[i].name = newName
          jValue[i].expiryY = newExpiryY
          jValue[i].expiryM = newExpiryM
          jValue[i].expiryD = newExpiryD
          jValue[i].phone = newPhone
          setSearchPhone(newPhone)
          setMembers(jValue[i])
          await AsyncStorage.setItem('user', JSON.stringify(jValue))
          getData()
          setEdit(!edit)
          
        }
      }
    
    } catch(e) {
      console.log("error!")
    }
}

    const loading = () => {
      setTimeout(
        load()
      , 3000)
    }

    const load = () => {
     setLoader(false)
     setViewResult(true)
    }
  useEffect(() => {
    getData()
   }, []);

  return (
    <View style={styles.container}>
     
     <View style={{display:"", flexDirection:"", marginTop:20}}>
     
      <TextInput
        value={searchPhone}
        keyboardType="numeric"
        maxLength={10}
        autoFocus
        placeholder="Enter phone number"
        placeholderTextColor="#123456"
        style={{ height: 50, width: "90vw", marginLeft:"5vw", fontSize:17, backgroundColor:"white", border:"none", textAlign:"center", borderRadius:"15px"}}
        onChangeText={(newText) => {
          setSearchPhone(newText)
          if(newText.length==10){
            setLoader(true)
            loading()
            let member =  allMembers.filter(
              (members) => members.phone == newText
            )
            let len = member.length
            
           if(len<1){
            setMembers([])
            // setTimeout(setLoader(false), 3000)
            setShowMember(false)
           }
           else{
            setSearchPhone(member[0].phone)
            setNewName(member[0].name)
            setNewPhone(member[0].phone)
            setNewExpiryD(member[0].expiryD)
            setNewExpiryM(member[0].expiryM)
            setNewExpiryY(member[0].expiryY)
            setMembers(member[0])
            setShowMember(true)
            // setTimeout(setLoader(false), 3000)
           }
           console.log(member)
         
          }else{
            setMembers([])
            setShowMember(false)
            // setTimeout(setLoader(false), 3000)
          }
        }}
        onValueChange={(itemValue, itemIndex) => setShortCode(itemValue)}
      >
        
      </TextInput>

      {
        showMember && viewResult &&
        <div 
        style={{
          marginTop:"20vh",
          backgroundColor:"white",
          height:"250px",
          marginLeft:"5vw",
          width:"90vw",
          marginRight:"5vw",
          padding:"",
          display:"flex",
          flex:1,
          alignItems:"center",
          justifyContent:"center",
          shadowColor: '#123456',
          shadowOffset: {width: 0, height: 1},
          shadowOpacity: 1,
          shadowRadius: 15,
          marginBottom:50,
          border:"1px solid #e6e6f2",
          borderRadius:"30px",
        }}
    >
      <div
      style={{
        display:"flex",
        flexDirection:"column",
        marginTop:10,
      }}
      >
        <div style={{ marginLeft:"90%", cursor:"pointer" }} onClick={()=>{
        {
          if(edit){
            if(newName && newExpiryY && newExpiryM && newExpiryD){
              replaceUserData()
            }else{
              alert("Please fill all fields!")
            }
          }else{
            setEdit(!edit)
          }
        }
        }}>
        {
          edit && 
          <AntDesign name="checkcircle" size={24} color="green" style={{marginLeft:15}}/>
        }
        {
          !edit && 
          <FontAwesome5 name="user-edit" size={24} color="#123456" style={{marginLeft:15}}/>
        }
        </div>
     <div
     style={{
      backgroundColor:"#123456",
      width:"60px",
      height:"60px",
      margin:"0 auto",
      borderRadius:"100%",
      textAlign:"center",
      display:"flex",
      alignItems:"center",
      justifyContent:"center",
      marginTop:20
     }}
     >
      <span 
      style={{
        color:"white",
        fontSize:30
      }}
      >{members.name.substring(0, 1)}</span>
     </div>
     <div style={{margin:"0 auto", marginTop:"-7px"}}>
      {
        (
         members.expiryD >= new Date().getDate() && 
         members.expiryM >= new Date().getMonth() &&
         members.expiryY >= new Date().getFullYear())
         ?
         <>
         <Octicons name="dot-fill" size={44} color="green" />
         </>
         :
         <>
         <Octicons name="dot-fill" size={44} color="red" />
         </>
      }
     </div>

   <div style={{}}>
     {
      !edit &&
      <h2 style={{ marginLeft:20}}>
      <Ionicons name="person-circle-sharp" size={24} color="black" />
      &nbsp;&nbsp;
      {members.name}
        </h2>
     }

{
      edit &&
      <TextInput
      maxLength={20}
      autoFocus
      value={newName}
      placeholder="eg: Abe Kebe"
      placeholderTextColor="#e6e6ff"
      style={{ height: 30, width: "100%", backgroundColor:"white", border:"none", textAlign:"left", paddingLeft:5, borderRadius:"10px", border:"1px solid black", marginBottom:10}}
      onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
      onChangeText={(newText) => setNewName(newText)}
    ></TextInput>
     }

     
     </div>

     <div style={{marginTop:"-20px"}}>
      {
        !edit && 
        <h2 style={{ marginLeft:20}}>
        <Feather name="phone-call" size={24} color="black" />
         &nbsp;&nbsp;
         {members.phone}
          </h2>
      }

      {
        edit && 
        <TextInput
        maxLength={10}
        value={newPhone}
        placeholder="eg: 09537147**"
        placeholderTextColor="#e6e6ff"
        style={{ height: 30, width: "100%", backgroundColor:"white", border:"none", textAlign:"left", paddingLeft:5, borderRadius:"10px", border:"1px solid black", marginTop:20}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setNewPhone(newText)}
      ></TextInput>
      }
   
     </div>

     <div style={{marginTop:"-20px", marginLeft:20, marginBottom:20}}>
      {
        !edit &&
        <h2>
        <MaterialIcons name="date-range" size={24} color="black" />
        &nbsp;&nbsp;
        {members.expiryY+'/'+members.expiryM+'/'+members.expiryD+' - '+members.expiryY+'/'+members.expiryM+'/'+members.expiryD+'  '}
         </h2>
      }

      {
        edit &&
        <View style={{display:"flex", flexDirection:"row", marginTop:20}}>

      <View style={{display:"flex", flexDirection:"column"}}>

      <Text style={{marginLeft:"-4vw", fontSize:17, marginBottom:10, marginTop:10}}>DD</Text>
      <TextInput
        placeholder="eg: 01"
        placeholderTextColor="#e6e6ff"
        value={newExpiryD}
        maxLength={2}
        keyboardType="numeric"
        style={{ marginLeft:"-4vw", height: 30, width: "20vw", border:"1px solid black", backgroundColor:"white", textAlign:"left", paddingLeft:10,  borderRadius:"10px"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setNewExpiryD(newText)}
      >
        
      </TextInput>
      </View>

      <View style={{display:"flex", flexDirection:"column", marginLeft:"3vw"}}>

      <Text style={{fontSize:17, marginBottom:10, marginTop:10}}>MM</Text>
      <TextInput
        placeholder="eg: 12"
        placeholderTextColor="#e6e6ff"
        keyboardType="numeric"
        maxLength={2}
        value={newExpiryM}
        style={{ height: 30, border:"1px solid black",  width: "14vw", backgroundColor:"white", textAlign:"left", paddingLeft:10, borderRadius:"10px"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setNewExpiryM(newText)}
      >
        
      </TextInput>
      </View>

      <View style={{display:"flex", flexDirection:"column", marginLeft:"10vw"}}>

      <Text style={{fontSize:17, marginBottom:10, marginTop:10}}>YYYY</Text>
      <TextInput
      placeholder="eg: 2024"
      placeholderTextColor="#e6e6ff"
       keyboardType="numeric"
        value={newExpiryY}
        maxLength={4}
        style={{ height: 30, border:"1px solid black", width: "30vw", backgroundColor:"white", textAlign:"left", paddingLeft:10, borderRadius:"10px"}}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        onChangeText={(newText) => setNewExpiryY(newText)}
      >
        
      </TextInput>
        </View>
      </View>
      }
    
     </div>

      </div>
    </div>
      }

        {
          loader &&
          <Image source={require('./Loading2.gif')} 
            style={{
            height:100, 
            width:100, 
            display:"flex", 
            justifyContent:"center", 
            borderRadius:"100%",
            margin:"0 auto",
            backgroundColor:"transparent"
            }} 
          />
        }
  

      {
        !showMember && phone.length>9 && viewResult &&
        <div style={{
          marginTop:"20vh",
          backgroundColor:"white",
          height:"250px",
          marginLeft:"5vw",
          width:"90vw",
          marginRight:"5vw",
          padding:"",
          display:"flex",
          flex:1,
          alignItems:"center",
          justifyContent:"center",
          shadowColor: '#123456',
          shadowOffset: {width: 0, height: 1},
          shadowOpacity: 1,
          shadowRadius: 15,
          marginBottom:50,
          border:"1px solid #e6e6f2",
          borderRadius:"30px",
        }}>
          <h3>No member found!</h3>
        </div>
      }
         
       
    </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // paddingTop: 40,
    // paddingLeft: 20,
    // paddingRight: 20,
    // alignItems: "start"
  }
});

export default App;

