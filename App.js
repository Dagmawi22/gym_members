import * as React from "react";
import { View, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import HomeScreen from "./screens/Home";
import AboutScreen from "./screens/Profile";
import OtherScreen from "./screens/Members";
import AddScreen from "./screens/Add";
import CheckScreen from "./screens/Check";
import MemberScreen from "./screens/Members";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: 'Bezaw',
          headerStyle: {
            backgroundColor: '#123456',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
        <Stack.Screen name="Profile" component={AboutScreen} />
        <Stack.Screen 
        component={MemberScreen}
        name="MembersScreen" 
        options={{
          title: 'All Members',
          headerStyle: {
            backgroundColor: '#123456',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} 
        />
        <Stack.Screen 
        name="AddMember" 
        component={AddScreen} 
        options={{
          title: 'Add Member',
          headerStyle: {
            backgroundColor: '#123456',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
        />
        <Stack.Screen 
        name="CheckMember" 
        component={CheckScreen}
        options={{
          title: 'Search a member',
          headerStyle: {
            backgroundColor: '#123456',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} 
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}